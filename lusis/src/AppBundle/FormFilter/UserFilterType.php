<?php

namespace AppBundle\FormFilter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;  
use Symfony\Component\Form\Extension\Core\Type\EmailType;


class UserFilterType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options) 
    {
        $builder->add('email', null, ['label' => 'user.email',
                'required' => false,])
            ->add('firstname', \Symfony\Component\Form\Extension\Core\Type\TextType::class, array(
                'label' => 'user.firstname',
                'required' => false,
            ))
            ->add('lastname', \Symfony\Component\Form\Extension\Core\Type\TextType::class, array(
                'label' => 'user.lastname',
                'required' => false,
            ))
            ->add('zipcode', \Symfony\Component\Form\Extension\Core\Type\TextType::class, array(
                'label' => 'user.zipcode',
                'required' => false,
            ))
            ->add('city', \Symfony\Component\Form\Extension\Core\Type\TextType::class, array(
                'label' => 'user.city',
                'required' => false,
            ));
    }
    
}
