<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @UniqueEntity("email")
 */
class User extends BaseUser 
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
 
     
    /**
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Your last name must be at least {{ limit }} characters long",
     *      maxMessage = "Your last name cannot be longer than {{ limit }} characters"
     * )
	 * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    protected $lastname;
    
    /**
     * @var string $lastname
     * 
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    protected $firstname;
    /**
     * @var string $street
     * 
     * @ORM\Column(name="street", type="string", length=255, nullable=true)
     */
    protected $street;
    /**
     * @var string $city
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    protected $city;
    /**
     * @var string $zipcode
     * @ORM\Column(name="zipcode", type="string", length=255, nullable=true)
     */
    protected $zipcode;
    
    /**
     * @var string $phone
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    protected $phone;	    
    
    public function __construct()
    {
        parent::__construct();
    }

    function getLastname() {
        return $this->lastname;
    }

    function getFirstname() {
        return $this->firstname;
    }

    function getPhone() {
        return $this->phone;
    }

    function setLastname($lastname) {
        $this->lastname = $lastname;
    }

    function setFirstname($firstname) {
        $this->firstname = $firstname;
    }

    function setPhone($phone) {
        $this->phone = $phone;
    }

        
    function getStreet() {
        return $this->street;
    }

    function setStreet($street) {
        $this->street = $street;
    }

    function getCity() {
        return $this->city;
    }

    function getZipcode() {
        return $this->zipcode;
    }

    function setCity($city) {
        $this->city = $city;
    }

    function setZipcode($zipcode) {
        $this->zipcode = $zipcode;
    }

    public function getFullName(){
        return $this->getLastname().' '.$this->getFirstname();
    }
    
}
