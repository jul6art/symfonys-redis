<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class StandardController extends Controller
{
    
    protected function keepFilterForm($request, $form){
        $keepForm = ($request->getSession()->get($form->getName()) != NULL);//&&($request->query->has('sort')||$request->query->has('page'));
        if (!$keepForm){
            $request->getSession()->remove($form->getName());
        }
        return $keepForm;
    }
    
    protected function  responseOkAjax(){
        $message = 'OK';
        $response = new Response();
        $response->headers->set('Content-Type', 'application/html');
        $response->setContent($message);
        return $response;
    }
    
    protected function sendEmail(\Swift_Message $message) {            
        return $this->get('mailer')->send($message); 
    }
    
}
