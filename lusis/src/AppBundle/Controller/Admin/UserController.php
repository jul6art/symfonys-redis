<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * User controller.
 *
 * @Route("/admin/user")
 * @Security("has_role('ROLE_ADMIN')")
 */
class UserController extends AdminController
{
    protected function getFormFilterType() {
        return \AppBundle\FormFilter\UserFilterType::class;
    }
    protected function getFormType() {
        return \AppBundle\Form\UserType::class;
    }
    protected function getName() {
        return 'user';
    }
    protected function getType() {
        return \AppBundle\Entity\User::class;
    }
    
    /**
     * Creates a new entity.
     *
     * @Route("/new")
     * @Method({"GET","POST"})
     */
    public function newAction(Request $request)
    {
        $entity = $this->createEntity();
        $entity->setPlainPassword(uniqid());
        $form = $this->createForm($this->getFormType(),$entity);
        $form->handleRequest($request);
        
        if($request->isXmlHttpRequest()){
             
             if ($form->isSubmitted()) {
                if ($form->isValid()){
                    // Requete ajax
                    $this->saveEntity($entity);
                    $this->sendAccountCreationEmailToUser($entity);
                    $message = 'OK';
                    $response = new Response();
                    $response->headers->set('Content-Type', 'application/html');
                    $response->setContent($message);

                    $this->get('session')->getFlashBag()->add(
                        'success','entity.save.success'
                    );
                    return $response;
                } else {// NOT VALID
                    return $this->render('default/form.html.twig', array(
                        'edit_form' => $form->createView(),
                    ));
                }
             }
        } else {
            if ($form->isSubmitted() && $form->isValid()) {
                $this->saveEntity($entity);
                $this->sendAccountCreationEmailToUser($entity);
                $this->get('session')->getFlashBag()->add(
                    'success','entity.save.success'
                );
                return $this->redirectToRoute($this->getTemplatePrefix().$this->getName().'_index');
            }
        }
        
        return $this->render('admin/user/new.html.twig', array(
            'entity' => $entity,
            'edit_form' => $form->createView(),
            'new_route' => $this->getTemplatePrefix().$this->getName().'_new',
            'title' => 'new.'.$this->getName(),
            'index_route' => $this->getTemplatePrefix().$this->getName().'_index',
        ));
    }
        
    /**
     * Displays a form to edit an existing  entity.
     *
     * @Route("/edit/{id}")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, $id)
    {
        $repository = $this->getDoctrine()->getRepository($this->getType());    
        $entity = $repository->find($id);
        $isEnabledBeforeSave = $entity->isEnabled();
        
        $editForm = $this->createForm($this->getFormType(),$entity);
        $editForm->handleRequest($request);
        
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->saveEntity($entity);
            // user was not enabled and he's now enabled
            if ((!$isEnabledBeforeSave) &&($entity->isEnabled())){
                $this->sendActivateEmail($entity);
            }
            $this->get('session')->getFlashBag()->add(
                'success','entity.save.success'
            );
            if($request->isXmlHttpRequest()){
                $message = 'OK';
                $response = new Response();
                $response->headers->set('Content-Type', 'application/html');
                $response->setContent($message);
                return $response;
            } else {
                return $this->redirectToRoute($this->getTemplatePrefix().$this->getName().'_index');
            }
        }
        // if the form was submitted and the request is AJAX
        if ($editForm->isSubmitted() && $request->isXmlHttpRequest()){
            return $this->render('default/form.html.twig', array(
                        'entity' => $entity,
                        'edit_form' => $editForm->createView(),
                    ));
        }
        $deleteForm = $this->createDeleteForm($entity);
        
        return $this->render('admin/user/edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'edit_route' => $this->getTemplatePrefix().$this->getName().'_edit',
            'index_route' => $this->getTemplatePrefix().$this->getName().'_index'
        ));
   }
   
   
    /**
     * Enable user
     *
     * @Route("/enable/{id}")
     * @Method({"GET"})
     */
    public function enableAction(Request $request, $id)
    {
        $repository = $this->getDoctrine()->getRepository($this->getType());    
        $entity = $repository->find($id);
        
        $entity->setEnabled(true);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        //$this->sendActivateEmail($entity);
        
        $this->get('session')->getFlashBag()->add(
            'success','entity.enable.success'
        );
        return $this->redirectToRoute($this->getTemplatePrefix().$this->getName().'_index');
   }
   
   public function sendActivateEmail($user){

        // send email user
        $mailer = $this->get('mailer'); // Swift_Mailer
        $generator = $this->get('app_bundle.message.twig_mail_generator');

        $userMessage = $generator->getMessage('user_activation_user', array(
            'user' => $user
        ));

        $userMessage->setTo($user->getEmail());
        $mailer->send($userMessage);

   }
   
   /**
     * Disable user
     *
     * @Route("/disable/{id}")
     * @Method({"GET"})
     */
    public function disableAction(Request $request, $id)
    {
        $repository = $this->getDoctrine()->getRepository($this->getType());    
        $entity = $repository->find($id);
        
        $entity->setEnabled(false);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success','entity.disable.success'
        );
        return $this->redirectToRoute($this->getTemplatePrefix().$this->getName().'_index');
   }
   
    /**
     * Validate membership user
     *
     * @Route("/validate/{id}")
     * @Method({"GET"})
     */
    public function validateAction(Request $request, $id)
    {
        $repository = $this->getDoctrine()->getRepository($this->getType());    
        $entity = $repository->find($id);
        
        $entity->setMember(true);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success','entity.validate.success'
        );
        return $this->redirectToRoute($this->getTemplatePrefix().$this->getName().'_index');
   }
   
    protected function sendAccountCreationEmailToUser(User $user)
    {
        //PGA : the following, till the end, is taken from FOSUserBundle ResettingController
        if (null === $user->getConfirmationToken()) {
            $tokenGenerator = $this->get('fos_user.util.token_generator');
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }
        
        $this->get('fos_user.mailer')->sendResettingEmailMessage($user);
        $user->setPasswordRequestedAt(new \DateTime());
        $this->get('fos_user.user_manager')->updateUser($user); 
    }
    
}
