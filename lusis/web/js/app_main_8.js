/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

(function ($) {
    $(document).ready(function () {
        // FIX FOR INTERNET EXPLORER
        $.ajaxSetup({
        cache: false
        });

        hljs.initHighlightingOnLoad();

        // Datetime picker initialization.
        // See http://eonasdan.github.io/bootstrap-datetimepicker/
        $('[data-toggle="datetimepicker"]').datetimepicker({
            icons: {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-check-circle-o',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });
        
        $('form button:reset').click(function () {
            $('form')
                .find(':radio, :checkbox').removeAttr('checked').end()
                .find('textarea, :text, select').val('');
            $(".pams-select2").val('').trigger('change');
            return false;
        });
    });
    
    $(".pams-select2").select2();

    // Handling the modal confirmation message.
    $(document).on('submit', 'form[data-confirmation]', function (event) {
        var $form = $(this),
            $confirm = $('#confirmationModal');

        if ($confirm.data('result') !== 'yes') {
            //cancel submit event
            event.preventDefault();

            $confirm
                .off('click', '#btnYes')
                .on('click', '#btnYes', function () {
                    $confirm.data('result', 'yes');
                    $form.find('input[type="submit"]').attr('disabled', 'disabled');
                    $form.submit();
                })
                .modal('show');
        }
    });
    
    
  $(".modal-form").on("click", function(){
    url = $(this).attr("href");
    return showFormModal("#editform",url);
  });
  
  
  
function showFormModal(formName, url){
    $("#modal-wrapper").load(url,function(){
      $("#myModal").modal("show");
      $(formName+" button[type=submit]").on("click", function(){
      if($(formName).valid()){
          var data = new FormData($(formName)[0]);
          
          $.ajax({
            type        : 'POST',
            url         : $(formName).attr('action'),
            data        : data, //$(formName).serialize(),// does not work with file to upload
            dataType    :"html",
            contentType : false,
            processData : false,
            success     : function(response)
            {
              if (response =="OK"){
                window.location.reload();
              } else {
                $(formName+" .modal-body").html(response);
              }
            }
          });
       }           
        return false;
      });
    });
    return false;
}
})(window.jQuery);
