Application
-----------------------

To run this application you need to:
Configure parameter.yml and yml.dist (change the username,password and database config)
Run composer install
- php composer.phar install

Create the database: (if needed)
- php bin/console doctrine:database:create
Update the schema
- php bin/console doctrine:schema:update --force

Create a default superadmin:
- php bin/console fos:user:create admin --super-admin 
Give him ROLE_ADMIN
- php bin/console fos:user:promote admin  ROLE_ADMIN

Generate the assets:
- php bin/console assetic:dump

You can launch the application and connect.
