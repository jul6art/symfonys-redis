-- base users insertion
-- user: isername=user & passwd=user
-- admin: isername=admin & passwd=admin
INSERT INTO `user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`) VALUES
    (1, 'admin', 'admin', 'geoffreyk422@gmail.com', 'geoffreyk422@gmail.com', 1, 'hn68gy4juy8juy8h4g748t7jy8t7jk4uy', '$2y$13$qd1yNg785LS/leCE.B22VOaGSyzH6EPRorFLDD4w0e16Ijc5JdQsO', '2016-11-21 11:59:34', 'osef', NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}'),
    (2, 'test', 'test', 'a@b.d', 'a@b.d', 1, 'hn68gy4juy8juy8h4g748t7jy8t7jk4uy', '$2y$13$c9W7zJCUGDZPQrYLychhxu.LEnUSv3t7dxcRhXiDD0dnQQWMxefOy', '2016-11-18 18:18:12', NULL, '2016-11-18 18:19:56', 'a:0:{}'),
    (3, 'user', 'user', 'osef2@osef.com', 'osef2@osef.com', 1, 'hn68gy4juy8juy8h4g748t7jy8t7jk4uy', '$2y$13$jvp8UGMlRdIFTBoYdv/MqODgkChAvtCiBvBEZh8WY.0jlkEDku8p.', '2016-11-21 11:55:32', NULL, NULL, 'a:2:{i:0;s:9:"ROLE_USER";i:1;s:12:"ROLE_VISITOR";}');